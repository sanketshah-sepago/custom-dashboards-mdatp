"""
    Helper functions for the entire project
"""
import requests
import json
import pandas as pd
import time
from constants import credentials, queries, hyperparams


def setup_api():
    """
    Uses the credentials to get the Bearer Token.

    :return:
    reqURL : The URL to access MD-ATP data.
    headers : Headers required for accessing MD-ATP data.
    """
    appId = credentials.appId
    appSecret = credentials.appSecret
    aadUrl = credentials.aadUrl
    resourceAppIdUri = credentials.resourceAppIdUri

    body = {
        'resource' : resourceAppIdUri,
        'client_id' : appId,
        'client_secret' : appSecret,
        'grant_type' : 'client_credentials'
    }

    reqAAD = requests.post(aadUrl, body)
    jsonResponseAad = reqAAD.json()
    bearerToken = jsonResponseAad['access_token']
    credentials.headers = {"Authorization":"Bearer " + bearerToken}

def get_data_MDATP(query,type="train"):
    '''
    Takes the query as input and returns the data as output based on the type.
    :param query: The query for fetching data
    :param type: The training or testing type.
    :return: data fetched for the query.
    '''
    if type == "train":
        query = query.replace("<days>", "-" + str(hyperparams.training_window) + "d")
    elif type == "test":
        query = query.replace("<days>", "-" + str(hyperparams.testing_window) + "d")
    body = {"Query": query}
    post_request = requests.post(credentials.reqURL,headers=credentials.headers,data=json.dumps(body))
    data = pd.DataFrame(post_request.json()['Results'])
    return data

def make_device_dataframe():
    '''
    Recognizes the list of devices in the given environment.
    :return:
    '''
    query_for_device_list = " DeviceInfo|summarize  dcount(Timestamp) by DeviceId "
    body  = {"Query": query_for_device_list}
    device_features = pd.DataFrame(requests.post(credentials.reqURL,headers=credentials.headers,data=json.dumps(body)).json()['Results'])
    device_features.set_index('DeviceId', inplace=True)
    return device_features

def fetch_data(row, device_features):
    '''
    :param row: The row from Feature Tuner
    :param reqURL: Requested URL for API call
    :param headers: headers for API access
    :param device_features: The dataframe to append new data.
    :return: The updated dataframe.
    '''
    action = row["action"]
    query = queries.query_dict[row['sub_factor']][action]
    '''
        Get training data
    '''
    data_training = get_data_MDATP(query,"train")
    try:
        data_training.set_index('DeviceId',inplace=True)
    except:
        device_features_all = device_features.copy()
        device_features_all[action]=data_training.iloc[0][action]
        data_training = device_features_all
    data_current = get_data_MDATP(query, "test")
    data_current.set_index('DeviceId',inplace=True)
    data_current.columns = data_current.columns + "_current"
    data_total = data_current.join(data_training,how='inner')[[action,action+'_current']]
    device_features = device_features.join(data_total,how='left')[[action,action+'_current']]
    print("Fetched data for ", action)
    time.sleep(50)
    return device_features


