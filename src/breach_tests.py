
def remove_whitelisted(original,whitelist):
    return set(original).difference(set(whitelist))


def threshold(df, col, col_current, threshold,score,whitelist,flag,breach_direciton):
    '''
    Performs a threshold test onto the given feature.
    :param df: The dataframe to perform test.
    :param col: The feature under test
    :param col_current: The name of the feature's current column.
    :param threshold: The threshold value for comparision
    :param score: The risk score
    :param whitelist: Not applicable here.
    :param flag: The on/off flag.
    :param breach_direciton: If "up" then only ">", if "down" then only "<" and if both then both.
    :return:
    '''
    score_col = col+"_score"
    df[score_col] = 0
    if flag == 0:
        df[score_col] = 0
        return df
    if breach_direciton=='both':
        df.loc[((df[col_current]-df[col]).abs()>df[col]*threshold), score_col] = score
    elif breach_direciton =='up':
        df.loc[((df[col_current]-df[col])>df[col]*threshold), score_col] = score
    elif breach_direciton =='down':
        df.loc[((df[col_current]-df[col])<df[col]*threshold), score_col] = score
    return df

def uniqueness(df,col,col_current,threshold,score,whitelist,flag,breach_direciton):
    '''
    Checks if a new value appeared for the given feature in the testing data.
    :param df: The dataframe to perform test.
    :param col: The feature under test
    :param col_current: The name of the feature's current column.
    :param threshold: Not applicable here.
    :param score: The risk score
    :param whitelist: The entities needed to whitelisted
    :param flag: The on/off flag.
    :param breach_direciton: Not applicable here.
    :return:
    '''
    score_col = col + "_score"
    if flag == 0.0:
        df[score_col] = 0
        return df
    df[col].loc[df[col].isnull()] = df[col].loc[df[col].isnull()].apply(lambda x: [])
    df[col_current].loc[df[col_current].isnull()] = df[col_current].loc[df[col_current].isnull()].apply(lambda x: [])
    df[score_col] = 0
    df[col] = df[col].apply(set)
    if whitelist != []:
        df[col_current] = df[col_current].apply(remove_whitelisted,whitelist=whitelist)
    df[col_current] = df[col_current].apply(set)
    df[score_col] = df.apply(lambda row : score if row[col_current].difference(row[col]) else 0,axis=1)
    return df

def across_board(df, col, col_current, threshold,score,whitelist,flag,breach_direciton):
    '''
    Compares the current value of the feature with the entire organizations average.
    :param df: The dataframe to perform test.
    :param col: The feature under test
    :param col_current: The name of the feature's current column.
    :param threshold: The threshold value for comparision
    :param score: The risk score
    :param whitelist: Not applicable here.
    :param flag: The on/off flag.
    :param breach_direciton: If "up" then only ">", if "down" then only "<" and if both then both.
    :return:
    '''
    score_col = col + "_score"
    df[score_col] = 0
    if flag == 0.0:
        df[score_col] = 0
        return df
    if breach_direciton=='both':
        df.loc[((df[col_current]-df[col]).abs()>df[col]*threshold),score_col] = score
    elif breach_direciton =='up':
        df.loc[((df[col_current]-df[col])>df[col]*threshold), score_col] = score
    elif breach_direciton =='down':
        df.loc[((df[col_current]-df[col])<df[col]*threshold), score_col] = score
    return df


def apply_breach_test(row):
    return globals()[row['breach_test']](row['data'], row['action'], row['action']+"_current", 0.2, row['score'], row['parsed_whitelist'], row['flag'], row["breach_direciton"])