import pandas as pd
import os
from src import data_collection, breach_tests, miscellaneous
feature_tuner = pd.ExcelFile(os.path.join("..","data","feature_tuner.xlsx")).parse("test")

'''
    1. Data collection
'''
data_collection.setup_api()
device_features = data_collection.make_device_dataframe()
feature_tuner['data']=feature_tuner.apply(data_collection.fetch_data, device_features=device_features, axis=1)
feature_tuner['parsed_whitelist']=feature_tuner.apply(miscellaneous.parse_whitelist, axis=1)

'''
    2. Breach Tests
'''
feature_tuner['post_scoring']=feature_tuner.apply(lambda row : breach_tests.apply_breach_test(row),axis=1)


'''
    3. Add text parsing logic
'''
feature_tuner = feature_tuner.apply( lambda row : miscellaneous.parse_breach(row),axis=1)

'''
    4. Combine all scores and breaches
'''
post_parsing_list = list(feature_tuner["post_parsing"])
post_parsing_combined = pd.concat(post_parsing_list,axis=1)
'''
    5. Group by sub_factor
'''
post_grouping = miscellaneous.group_by_sub_factor(feature_tuner,post_parsing_combined)
print(post_grouping)
