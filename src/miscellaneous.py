
def parse_whitelist(row):
    whitelist = []
    if row['whitelist'] == "None":
        return whitelist
    for item in row['whitelist'][1:-1].split("\""):
        if not (item == '' or item == ' '):
            whitelist = whitelist + [item]
    return whitelist

def replace_data(row,action,current_action):
    if row[action+"_score"] > 0:
        row[action+"_breach"] = row[action+"_breach"].replace("*"+action+"*",str(row[action])).replace("*"+current_action+"*",str(row[current_action]))
    else:
        row[action+"_breach"] = ""
    return row

def parse_breach(row):
    data = row["post_scoring"]
    action = row["action"]
    current_action = row["action"] +"_current"
    data[action+"_breach"] = row["text"]
    data = data.apply(lambda r : replace_data(r,action,current_action),axis=1)
    row["post_parsing"] = data
    return row


def group_by_sub_factor(feature_tuner,post_parsing_combined):
    sub_factor_action_map = feature_tuner.groupby(['sub_factor'])['action'].apply(list).to_dict()
    for sub_factor in sub_factor_action_map.keys():
        post_parsing_combined[sub_factor + "_score"] = 0
        post_parsing_combined[sub_factor + "_breach"] = [[] for _ in range(len(post_parsing_combined))]
        for action in sub_factor_action_map[sub_factor]:
            post_parsing_combined[sub_factor + "_score"] = post_parsing_combined[sub_factor + "_score"] + post_parsing_combined[action + "_score"]
    for sub_factor in sub_factor_action_map.keys():
        post_parsing_combined[sub_factor + "_breach"] = [[] for _ in range(len(post_parsing_combined))]
        sub_factor_columns = [action + "_breach" for action in sub_factor_action_map[sub_factor]]
        post_parsing_combined[sub_factor + "_breach"] = list(post_parsing_combined[sub_factor_columns].values)
    return post_parsing_combined