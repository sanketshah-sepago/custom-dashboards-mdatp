query_dict = {
    "Authentication":
        {

            'AverageFirstLoginTime': """DeviceLogonEvents
                                    | where Timestamp > now(<days>) 
                                    | extend date_part =  bin(Timestamp, 1d)
                                    | summarize  FirstLogin = min(Timestamp) by DeviceId , date_part
                                    | extend time_part = datetime_part("Hour", FirstLogin)*60 + datetime_part("Minute", FirstLogin)
                                    | summarize AverageFirstLoginTime = avg(time_part) by DeviceId """
        }
}